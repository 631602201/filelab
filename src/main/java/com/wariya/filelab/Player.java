/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.filelab;

/**
 *
 * @author wariy
 */
public class Player {
    
    private char symbol;
    private int win;
    private int lost;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getWin() {
        return win;
    }

    public void Win() {
        this.win++ ;

    }

    public int getLost() {
        return lost;
    }

    public void Lost() {
        this.lost++;

    }

    public int getDraw() {
        return draw;
    }

    public void Draw() {
        this.draw++ ;

    }
    
    @Override
    public String toString() {
        return "Player = " + symbol + ", win = " + win + ", lost = " + lost + ", draw = " + draw;
    }

    String getLoss() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


    
}
