/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.function.ObjIntConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author wariy
 */
public class ReadFriend {
    public static void main(String[] args) {
         FileInputStream fis = null;
        try
        {
            Friend Friend1 = null;
            Friend Friend2 = null;
            Friend Friend3 = null;
            File file = new File("friends.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Friend1 = (Friend) ois.readObject();
            Friend2 = (Friend) ois.readObject();
            Friend3 = (Friend) ois.readObject();
            System.out.println(Friend1);
            System.out.println(Friend2);
            System.out.println(Friend3);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally
        {
            try
            {
                fis.close();
            } catch (IOException ex)
            {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        
    }   
}
