/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.filelab;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author wariy
 */
public class WriteFriend {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try
        {
            Friend Friend1 = new Friend("Danudecch", 20, "0845430222");
            Friend Friend2 = new Friend("Chattamas", 20, "0982627881");
            Friend Friend3 = new Friend("Suphakorn", 21, "0844444444");
            File file = new File("friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(Friend1);
            oos.writeObject(Friend2);
            oos.writeObject(Friend3);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally
        {
            try
            {
                fos.close();
            } catch (IOException ex)
            {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
